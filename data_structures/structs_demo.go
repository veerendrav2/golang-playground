package main

import "fmt"

func main(){

	type Doctor struct {			// Struct definition
		id int
		name string
		companions [4]string
	}

	aDoctor := Doctor{				// Struct assignment
		id: 134,
		name: "doc",
		companions: [4]string {
			"doc1",
			"doc2",
		},
	}

	fmt.Println("The capacity of companions in Doctor struc ->", cap(aDoctor.companions))		// Access members of struct

	aActor := struct{name string id int}{name: "veerendra", id: 123}
	fmt.Println(aActor)
}