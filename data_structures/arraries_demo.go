package main

import (
	"fmt"
)

func main() {
	grades := [3]int{12, 2, 3}            // Decloration & assignment
	grades1 := [...]float32{1.2, 23.1, 3} // Decloration & assignment with short hand notation
	var students [3]string                // Decloration with size
	students[0] = "Bot1"                  // Assign string at 0th index

	//	var a [3][3]int = [3][3]int{ [3]{1, 2, 3}, [3]{4, 5, 6}, [3]{1, 2, 3} }

	b := [...]int{1, 2, 3}
	c := &b // 								c is pointing to b
	f := b  // 								New memory is allocated for f and copies b to f

	fmt.Printf("The grades are %v and its type %T\n", grades, grades)
	fmt.Printf("The grades are %v and its type %T\n", grades1, grades1)
	fmt.Printf("The grades are %v and its type %T\n", students, students)
	fmt.Printf("Student No.1: %v\n", students[0])
	fmt.Printf("Display b-> %v\n", b)

	// ----------------ARRAY FUNCTIONS---------------------
	fmt.Printf("The length is %v\n", len(students))

	// --------------- SLICES ---------------------
	x := []int{1, 2, 3} // Slice decloration & assignment
	y := x              // Slices are by default, reference types. y points to x
	g := []int{}
	fmt.Printf("The g is %v and type is %T\n", g, g)
	fmt.Printf("The c,f,y value is %v %v %v\n", c, f, y)
	fmt.Printf("The x lenght is %v\n", len(x))
	fmt.Printf("The x lenght is %v\n", cap(x))
	// Slicing supports like in Python

	x = append(x, 12, 3)
	fmt.Printf("x after append is %v\n", x)

	z := make([]int, 3, 10) // Slice decloration with size and capacity
	fmt.Printf("The value of z is %v\n", z)
	fmt.Printf("The z capacity is %v\n", cap(z))
}
