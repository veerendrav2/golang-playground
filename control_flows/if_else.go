package main

import "fmt"


func main(){
	myNumber := 10
	if myNumber < 10 {
		fmt.Println("Too low!")
	}

	if myNumber == 10 {
		fmt.Println("No!")
	} else if myNumber == 11 {			// else if syntax
		fmt.Println("Correct")
	}
}