package main

import (
	"fmt"
)

func main(){
	switch 80 {
	case 90, 80:
		fmt.Println("THe case is 90 and 80")

	case 1:
		fmt.Println("The case is 1")

	default:
		fmt.Println("The default ")
	}


	switch i := 1 + 3;i {
	case 10:
		fmt.Println("Less than 10")
	case 20:
		fmt.Println("Greater than 10")
	default:
		fmt.Println("The value is", i)
	}

	i := 20
	switch {
	case i <= 20:
		fmt.Println("Less than 20")
		// Implicite break for every case!!
		fallthrough			// To make continue to next case.
	case i < 30:
		fmt.Println("Greater than 20")
		fallthrough			// It will continue! to next regardless of condition
	case i ==100:
		fmt.Println("This is 100")
	default:
		fmt.Println("The value is", i)
	}

}