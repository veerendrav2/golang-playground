package main

import "fmt"

func main(){
	for i:=0; i<=5; i++ {
		fmt.Println(i)
	}

	x := 0
	for ; x<3; x++ {		// No assignment in for block, allow access from outside scope
		fmt.Println(x)
	}

	for i:=0; i<3; {
		fmt.Println(x)
		i++								// Can also put incrementor here
	}

	// for ;; {			// Infinit loop!
	// 	fmt.Println("Hello!")
	// }

	myList := []int{1,2,3,5}
	for k,v := range myList {
		fmt.Println("Key and Value -> ", k, v)
	}

	myMap := map[float32]string{
		1.2: "test",
		2.3: "test2",
	}
	for k := range myMap {
		fmt.Println("Only Key ->", k)
	}

	for _, v := range myMap {
		fmt.Println("Only Value->", v)
	}

}