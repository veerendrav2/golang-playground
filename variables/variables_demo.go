package main

import (
	"fmt"
)

var (
	k int     = 30
	l float32 = 40.0
)

func main() {
	var i int
	var k float32
	var m int
	i = 10
	j := 11
	k = 12

	fmt.Println("Hello World!")
	fmt.Printf("The Values of i is %v\n", i)
	fmt.Printf("The Values of j is %v %T\n", j, j)
	fmt.Printf("The Values of k is %v\n", k)
	fmt.Printf("The Values of l is %v with datatype is %T\n", l, l)
	fmt.Printf("The value of m is %v and type %T\n", m, m)

	var s1 string = "This is 1"
	var s2 string = "This is 2"
	fmt.Printf(s1 + s2) // String Concatination
}
