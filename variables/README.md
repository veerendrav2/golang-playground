# Varaibles
Decloration Example
```
var i int
var i int = 10
i := 10
```
If the variable scope is package level, then it has to be declared fully
```
var i int
var i int = 10
```
* Variable has be used when it is declared
* Variable starts with upper case is global scope
* Variable starts with lower case is package level
* Pascal or camelCase naming covenstion
* Capitailize acronyms(HTTPS, URL, etc)
* Explicit type casting even for same type with different size(e.g. int8)

## Primitive Datatypes
### Boolean 
* Not an alias for other type(e.g. int)
```
var n bool = true
var n bool // By default it false/0
```
### Integer
Unsigned Int
* init  -> Platform dependent
* int8
* int16
* int32
* int64

Signed Int
```
var n uint = 02
``` 
* uinit  -> Platform dependent
* uint8(byte)
* uint16
* uint32

No implicit type casting even for different sizes of integers 
```
var a int = 10
var b int8 = 12
fmt.Println(a + int(b)) 
```

### Float
* float32
* floar64

### Complex
* complex64
* complex128

### String
* Array of bytes
* Immutable
* UTF-8
```
s := "This is a string!"         // Implicit. GO treats as "string" because of double quotes
fmt.Printf("%v, %T", s[2], s[2]) // Returns ASCII value of the letter `i` with type of `int8`
```
### Rune
* UTF-32
* Internally, it is alias for int32
```
r := 'This is rune data type!'  // Implicit. GO treats as "rune" because of single quotes
var x rune = "This is decloration"
fmt.Printf("%v %T", r, r)
```

## Constants

* Constant are defined at compile time, not run time
* Constant variables are replaced with value in the program
```
package main

import (
    "fmt",
    "math"
)
func main(){
    const myConst int = 10
    const myConst2 = 12 // <-- Short hand notation to declore int variable

    const myConst int = math.Sin(1.57) // This is not allowed, since function will execute on runtime
    myConst = 12 // NOT allowed to change, since it is constant
    
    fmt.Printf("%v, %T\n", myConst, myConst)
}
```
