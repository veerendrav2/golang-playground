package main

import "fmt"

func main() {
	a := 8 // 2^3
	b := 5
	fmt.Println("Add", (a + b))
	fmt.Println("Sub", (a - b))
	fmt.Println("Mul", (a * b))
	fmt.Println("Mod", (a % b))

	// Bit Operators
	fmt.Println(a & b)  // AND
	fmt.Println(a | b)  // OR
	fmt.Println(a ^ b)  // Exclusive OR
	fmt.Println(a &^ b) // AND NOR

	// Bit Shiffting
	fmt.Println(a >> 3) // 2^3 * 2^3 = 2^8
	fmt.Println(a << 3) // 2^3 / 2^3 = 2^0

}
