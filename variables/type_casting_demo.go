package main

import (
	"fmt"
	"strconv"
)

func main() {
	var i int = 10
	fmt.Printf("The type before casting is %v %T\n", i, i)

	var x float32
	x = float32(10)
	fmt.Printf("The type after casting is %v %T\n", x, x)

	var y string
	y = strconv.Itoa(i)
	fmt.Printf("The type after casting is %v %T", y, y)
}
