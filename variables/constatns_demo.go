package main

import (
	"fmt"
)

const (
	_ = iota + 1 // iota is enumator
	cat
	dog
	snake
)

const (
	_ = iota << 1 // Can also bitshifting
	cat1
	dog2
	snake2
)

func main() {
	const myConst int = 10
	const myConst2 = 12 // <-- Short hand notation to declore int variable

	//const myConst int = math.Sin(1.57) // This is not allowed, since function will execute on runtime
	//myConst = 12                       // NOT allowed to change, since it is constant

	fmt.Printf("%v, %T\n", myConst, myConst)
	fmt.Printf("%v, %T\n", myConst2, myConst2)
	fmt.Printf("%v, %T", dog, dog)

}
